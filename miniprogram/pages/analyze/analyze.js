// pages/databaseGuide/databaseGuide.js

const app = getApp()
import * as echarts from '../../components/ec-canvas/echarts';


function setOption(chart, itemId) {
    console.log("setOption")
    // let top10ScoreList = {}
    let top10 = []
    //获取当前健康信息
    wx.cloud.callFunction({
        name: 'getExamScoreList_rdl',
        data: {
            itemId: itemId,
            max: 10,
            page: 1
        },
    }).then((res) => {
        console.log('[云函数] [getExamScoreList_rdl--top10] : ', res)

        let top10List = []
        //横坐标
        let dataX = []
        //纵坐标
        let dataY = []
        let max = 50;
        let min = 50;

        res.result.itemScoreList.data.reverse().forEach(score => {
            top10List.push(score)
            dataX.push(score.time)
            dataY.push(score.score)

            //计算坐标轴最大值
            if (score.score > max) {
                //取个位
                let max_ = parseInt(score.score % 10);
                max = (score.score - max_) + 10
                console.log("取个位", max_, max)
            }
            //计算坐标轴最小值
            if (score.score < min) {
                //取个位
                let min_ = parseInt(score.score % 10);
                min = (score.score - min_) - 10
                if (min < 0) {
                    min = 0
                }
            }
        })

        console.log("max min dataX  dataY", max, min, dataX, dataY)


        const option = {
            grid: {
                left: 10,
                right: 10,
                bottom: 10,
                top: 10,
                containLabel: true
            },
            xAxis: {
                type: 'category',
                data: dataX,
                axisLabel: {
                    interval: 0,
                    rotate: 35,
                    show: true,
                    textStyle: {
                        color: '#d3ebff',  //更改坐标轴文字颜色
                        fontSize: 7      //更改坐标轴文字大小
                    }
                },
                axisLine: {
                    lineStyle: {
                        color: "#fff"
                    }
                }
            },
            yAxis: {
                min: min,
                max: max,
                type: 'value',
                axisLine: {
                    lineStyle: {
                        color: "#fff"
                    },
                },
                axisLabel: {
                    show: true,
                    textStyle: {
                        color: '#d3ebff',  //更改坐标轴文字颜色
                        fontSize: 9      //更改坐标轴文字大小
                    }
                },
                splitLine: {    //网格线
                    lineStyle: {
                        type: 'dotted',    //设置网格线类型 dotted：虚线   solid:实线
                        color: "#afafff",
                        opacity: 0.4
                    },
                    show: true //隐藏或显示
                }

            },
            series: [{
                data: dataY,
                type: 'line'
            }]
        };
        chart.setOption(option);
    })


}


Page({


    data: {

        itemId: "7498b5fe5f4d0a6a00b7293c5997e393",
        itemName: "语文",
        top10ScoreList: [],
        scoreList: [],
        page:1,
        ec: {
            lazyLoad: true
        }
    },
    toScoreRecord(e) {
        console.log("点击分数跳转", e, e.currentTarget, e.currentTarget.dataset, e.currentTarget.dataset.item)
        let score = e.currentTarget.dataset.item
        wx.navigateTo({
            url: '/pages/record/record?itemScoreId=' + score._id + "&itemName=" + score.itemName,
        })
    },

    toRecord() {
        wx.navigateTo({
            url: '/pages/record/record?itemId=' + this.data.itemId + "&itemName=" + this.data.itemName,
        })
    },
    onLoad(option) {
        console.log(option)
        this.setData({
            itemId: option.itemId,
            itemName: option.itemName
        })
    },
    //

    onShow() {
        this.setData({
            scoreList: [],
            page: 1,
        })
        this.getScoreList()
        this.refreshChart()
    },

    getScoreList() {
        wx.cloud.callFunction({
            name: 'getExamScoreList_rdl',
            data: {
                itemId: this.data.itemId,
                max: 10,
                page: this.data.page
            },
        }).then((res) => {
            console.log('[云函数] [getExamScoreList_rdl] : ', res)
            let list = this.data.scoreList
            res.result.itemScoreList.data.forEach(score => {
                list.push(score)
            })
            let page_ = this.data.page
            this.setData({
                scoreList: list,
                page: page_ + 1
            })

            console.log("scoreList", this.data.scoreList)
        })
    },


//图表相关
    refreshChart(e) {
        this.ecComponent = this.selectComponent('#health-chart');
        this.ecComponent.init((canvas, width, height, dpr) => {
            // 获取组件的 canvas、width、height 后的回调函数
            // 在这里初始化图表
            const chart = echarts.init(canvas, null, {
                width: width,
                height: height,
                devicePixelRatio: dpr // new
            });
            //设置数据
            setOption(chart, this.data.itemId);
            // 将图表实例绑定到 this 上，可以在其他成员函数（如 dispose）中访问
            this.chart = chart;
            this.setData({
                isLoaded: true,
                isDisposed: false
            });
            // 注意这里一定要返回 chart 实例，否则会影响事件处理等
            return chart;
        });
    },
    //触底响应函数
    onReachBottom() {
        console.log("触底了")
        this.getScoreList()
    },

})
