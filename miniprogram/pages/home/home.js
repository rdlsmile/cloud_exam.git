// pages/databaseGuide/databaseGuide.js

const app = getApp()


Page({


    data: {
        show: false,
        showAddItem: false,
        addItemUrl: "",
        dialogItemId: "",
        dialogItemName: "",



        showEdit: false,
        editItemId: "",
        itemList: "",
        imgIds: [
            "cloud://cloud-qioch.636c-cloud-qioch-1302935104/exam/1.png"
            , "cloud://cloud-qioch.636c-cloud-qioch-1302935104/exam/2.png"
            , "cloud://cloud-qioch.636c-cloud-qioch-1302935104/exam/3.png"
            , "cloud://cloud-qioch.636c-cloud-qioch-1302935104/exam/4.png"
            , "cloud://cloud-qioch.636c-cloud-qioch-1302935104/exam/5.png"
            , "cloud://cloud-qioch.636c-cloud-qioch-1302935104/exam/6.png"
            , "cloud://cloud-qioch.636c-cloud-qioch-1302935104/exam/7.png"
            , "cloud://cloud-qioch.636c-cloud-qioch-1302935104/exam/8.png"
            , "cloud://cloud-qioch.636c-cloud-qioch-1302935104/exam/9.png"
            , "cloud://cloud-qioch.636c-cloud-qioch-1302935104/exam/10.png"
            , "cloud://cloud-qioch.636c-cloud-qioch-1302935104/exam/11.png"
            , "cloud://cloud-qioch.636c-cloud-qioch-1302935104/exam/12.png"
        ],
        imgList: []
    },

    onShow() {
        this.reLoadData()
    },
    onLoad(){
        wx.showShareMenu()
    },


    //获取科目列表
    getExamItemList() {
        wx.cloud.callFunction({
            name: 'getExamItemList_rdl',
            data: {},
        }).then((res) => {
            console.log('[云函数] [getExamItemList_rdl] : ', res)
            this.setData({
                itemList: res.result.itemList,
            });
            this.checkItem();
        })
    },

    //检查是否有科目，如果没有的话给出默认值
    checkItem() {
        if (this.data.itemList.length === 0) {
            //初始化科目
            console.log("需要初始化科目列表")
            this.initItems()
        }
    },
    initItems() {
        wx.cloud.callFunction({
            name: 'addExamItem_rdl',
            data: {type: "init"},
        }).then((res) => {
            console.log('[云函数] [addExamItem_rdl] : ', res)
            //完成之后刷新页面
            wx.reLaunch({
                url: "/pages/home/home",
            })
        })
    },
    addItem() {
        wx.cloud.callFunction({
            name: 'addExamItem_rdl',
            data: {name: "test"},
        }).then((res) => {
            console.log('[云函数] [addExamItem_rdl] : ', res)
            //完成之后刷新页面
            wx.reLaunch({
                url: "/pages/home/home",
            })
        })
    },

    //获取图片地址
    getFileUrl() {
        wx.cloud.getTempFileURL({
            fileList: this.data.imgIds
        }).then(res => {
            // get temp file URL
            console.log("urls", res.fileList)
            this.setData({
                imgList: res.fileList,
            });
        }).catch(error => {
            // handle error
        })
    },


    toAnalyze(e) {
        console.log("点击", e, e.currentTarget, e.currentTarget.dataset, e.currentTarget.dataset.item)
        let item = e.currentTarget.dataset.item
        wx.navigateTo({
            url: '/pages/analyze/analyze?itemId=' + item._id+"&itemName="+item.name,
        })
    },
    editShow(e) {
        let item = e.currentTarget.dataset.item
        this.setData({
            show:true,
            dialogItemId:item._id,
            dialogItemName:item.name,
        })
    },
    showAddItem(){
        this.setData({
            show:true,
            dialogItemId:null,
            dialogItemName:null,
        })
    },
    reLoadData() {
        this.getExamItemList()
        if(this.data.imgList.length<1){
            this.getFileUrl()
        }
    },


    openPop() {
        console.log("openPop")

    },
    closePop() {
        this.setData({
            show: false,
        })
    },
    afterEditItem(e) {
        console.log('[云函数] [ e.detail options] : ', e.detail, e.options)
        this.closePop()
        this.reLoadData()
    },

    bindDateChange: function (e) {
        console.log('picker发送选择改变，携带值为', e.detail.value)
        this.setData({
            date: e.detail.value
        })
    },
    //转发
    onShareAppMessage(res) {

        return {
            title: '乐考吧',
            path: '/pages/home/home'
        }
    },
    //收藏
    onAddToFavorites(res) {
        return {
            title: '乐考吧',
        }
    },
    //分享到朋友圈
    onShareTimeline(res) {
        return {
            title: '乐考吧',
        }
    }
})

