// pages/databaseGuide/databaseGuide.js

const app = getApp()


Page({


    data: {
        itemId: "",
        itemName: "",
        itemScoreId: "",
        time: "",
        type: "",
        score: "",
        note: "",
    },
    onLoad(option) {
        if (option.itemScoreId) {
            this.setData({
                itemScoreId: option.itemScoreId,
                itemName: option.itemName
            });
            //查询分数信息
            wx.cloud.callFunction({
                name: 'getExamScoreById_rdl',
                data: {id: this.data.itemScoreId},
            }).then((res) => {
                console.log('[云函数返回] [getExamScoreById_rdl] : ', res)
                if (res.result) {
                    this.setData({
                        itemId: res.result.data.itemId,
                        time: res.result.data.time,
                        type: res.result.data.type,
                        score: res.result.data.score,
                        note: res.result.data.note
                    })
                }
            })
        }
        if (option.itemId) {
            this.setData({
                itemId: option.itemId,
                itemName: option.itemName
            });
        }

    },
    bindDateChange(e) {
        console.log('picker发送选择改变，携带值为', e.detail.value)
        this.setData({
            time: e.detail.value
        })
    },
    bindType(e) {
        this.setData({
            type: e.detail.value
        })
    },

    bindScore(e) {
        this.setData({
            score: e.detail.value
        })
    },
    bindNote(e) {
        this.setData({
            note: e.detail.value
        })
    },
    getData() {
        if (!this.data.time) {
            wx.showToast({
                title: '请选择时间！',
                icon: 'none',
                duration: 1500
            })
            return
        }
        if (!this.data.type) {
            wx.showToast({
                title: '请填写考试类型！',
                icon: 'none',
                duration: 1500
            })
            return
        }
        if (parseFloat(this.data.score).toString() == "NaN") {
            wx.showToast({
                title: '请正确填写分数！',
                icon: 'none',
                duration: 1500
            })
            return
        }

        let data = {
            itemId: this.data.itemId,
            itemName: this.data.itemName,
            time: this.data.time,
            type: this.data.type,
            score: this.data.score,
            note: this.data.note,
        }

        return data
    },
    addScore() {
        let data = this.getData()
        if (!data) {
            return
        }
        wx.showLoading({title: '加载中',mask:true})
        wx.cloud.callFunction({
            name: 'addExamScore_rdl',
            data: data,
        }).then((res) => {
            wx.hideLoading()
            console.log('[云函数返回] [addExamScore_rdl] : ', res)
            if (res.result.code != "200") {
                wx.showToast({
                    title: res.result.message,
                    icon: 'none',
                    duration: 1500
                })
            } else {
                console.log('getCurrentPages()', getCurrentPages())
                wx.navigateBack({
                    delta: 1
                })
            }
        })
    },

    editExamScore() {
        let data = this.getData()
        if (!data) {
            return
        }
        wx.showLoading({title: '加载中',mask:true})
        data.id = this.data.itemScoreId
        wx.cloud.callFunction({
            name: 'editExamScore_rdl',
            data: data,
        }).then((res) => {
            wx.hideLoading()
            wx.navigateBack({
                delta: 1
            })
        })
    },

    delExamScore() {
        let data = {
            id: this.data.itemScoreId,
        }
        wx.showLoading({title: '加载中',mask:true})
        wx.cloud.callFunction({
            name: 'delExamScore_rdl',
            data: data,
        }).then((res) => {
            wx.hideLoading()
            wx.navigateBack({
                delta: 1
            })
        })
    },


    onShow() {
    },


})
