// pages/databaseGuide/databaseGuide.js

const app = getApp()


Component({
    properties: {
        itemId:{
            type: String,
            value: 'e'
        },
        itemName: {
            type: String,
            value: 'e'
        },
    },
    data: {
        name:""
    },
    methods: {
        onShow() {
        },

        bindItemName(e) {
            console.log( e.detail.value)
            this.setData({
                itemName: e.detail.value
            })
        },
        addItem() {
            wx.showLoading({title: '加载中',mask:true})
            wx.cloud.callFunction({
                name: 'addExamItem_rdl',
                data: {
                    name:this.data.itemName
                },
            }).then((res) => {
                wx.hideLoading()
                console.log('[云函数] [addExamItem_rdl] : ', res)
                //调用父组件
                this.triggerEvent('afterEditItem') //myevent 自定义名称事件，父组件中使用
            })
        },
        editItem() {
            wx.showLoading({title: '加载中',mask:true})
            wx.cloud.callFunction({
                name: 'editExamItem_rdl',
                data: {
                    id:this.data.itemId,
                    name:this.data.itemName
                },
            }).then((res) => {
                wx.hideLoading()
                console.log('[云函数] [editExamItem_rdl] : ', res)
                //调用父组件
                this.triggerEvent('afterEditItem') //myevent 自定义名称事件，父组件中使用
            })
        },
        delItem(){
            wx.showLoading({title: '加载中',mask:true})
            wx.cloud.callFunction({
                name: 'delExamItem_rdl',
                data: {
                    id:this.data.itemId,
                },
            }).then((res) => {
                wx.hideLoading()
                console.log('[云函数] [delExamItem_rdl] : ', res)
                //调用父组件
                this.triggerEvent('afterEditItem') //myevent 自定义名称事件，父组件中使用
            })
        },
        closeEditItem() {
            this.triggerEvent('afterEditItem')
        }
    },



})
//
// Page({
//
// properties: {
//         itemId:{
//             type: String,
//             value: 'e'
//         },
//         itemName: {
//             type: String,
//             value: 'e'
//         },
//     },
//     data: {
//         value: 0,
//         defValue: 63,
//     },
//     onShow() {
//         console.log(this.data.itemName, this.properties.itemName)
//     },
//
//     bindvalue: function (e) {
//         // console.log(e)
//         this.setData({
//             value: e.detail.value
//         })
//     },
//     addWeight() {
//         console.log("选中体重", this.data.value)
//         console.log(this.data.itemName, this.properties.itemName)
//         this.triggerEvent('afterEditItem') //myevent 自定义名称事件，父组件中使用
//     },
//     closeEditItem() {
//         this.triggerEvent('afterEditItem')
//     }
//
//
// })
