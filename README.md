

# 项目名称
乐考吧

# 项目介绍
你是不是已经记不起来你家孩子一年前、一个月前、甚至一周前的考试成绩了？来乐考吧，还你一个有历史的成绩。
乐考吧致力于为父母孩子提供一个记录成绩的有历史的小程序。

# 项目效果截图

- <img src="https://gitee.com/rdlsmile/cloud_exam/raw/master/img/pages/1.png" width="375">

- <img src="https://gitee.com/rdlsmile/cloud_exam/raw/master/img/pages/2.jpg" width="375">

- <img src="https://gitee.com/rdlsmile/cloud_exam/raw/master/img/pages/3.jpg" width="375">

- <img src="https://gitee.com/rdlsmile/cloud_exam/raw/master/img/pages/4.jpg" width="375">


# 小程序二维码

- <img src="https://gitee.com/rdlsmile/cloud_exam/raw/master/img/guide/logo.jpg" width="375">

# 部署教程
### 下载代码

- <img src="https://gitee.com/rdlsmile/cloud_exam/raw/master/img/guide/clone.png" width="375">

### 导入微信开发者工具

- <img src="https://gitee.com/rdlsmile/cloud_exam/raw/master/img/guide/import.png" width="375">

### 修改appId

- <img src="https://gitee.com/rdlsmile/cloud_exam/raw/master/img/guide/appId.png" width="375">

### 选择云环境

- <img src="https://gitee.com/rdlsmile/cloud_exam/raw/master/img/guide/env.png" width="375">

### 上传云函数

- <img src="https://gitee.com/rdlsmile/cloud_exam/raw/master/img/guide/upload.png" width="375">

### 启动页面

- <img src="https://gitee.com/rdlsmile/cloud_exam/raw/master/img/guide/open.png" width="375">


# 开源许可证标注

MIT License