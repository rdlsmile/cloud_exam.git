// 云函数入口文件
const cloud = require('wx-server-sdk')
// 初始化 cloud
cloud.init({
    // API 调用都保持和云函数当前所在环境一致
    env: cloud.DYNAMIC_CURRENT_ENV
})

// 云函数入口函数
exports.main = async (event, context) => {
    const db = cloud.database()

    let max = 10
    if (event.max) {
        max = event.max
    }
    let page = 1
    if (event.page) {
        page = event.page
    }
    if (page < 1) {
        page = 1
    }

    console.log((page - 1) * max, max, event.itemId)
    const itemScoreList = await db.collection('exam_score')
        .where({
            openId: event.userInfo.openId,
            itemId: event.itemId,
        })
        .orderBy('time', 'desc')
        .skip((page - 1) * max).limit(max).get().then(res => {
            console.log("查询分数返回", res)
            return res
        })

    return {
        itemScoreList: itemScoreList,
    }
}

