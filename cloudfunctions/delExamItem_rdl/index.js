// 云函数入口文件
const cloud = require('wx-server-sdk')
// 初始化 cloud
cloud.init({
    // API 调用都保持和云函数当前所在环境一致
    env: cloud.DYNAMIC_CURRENT_ENV
})

// 云函数入口函数
exports.main = async (event, context) => {
    const db = cloud.database()

    //删除分数记录
    const resultScore = await db.collection('exam_score').where({
        itemId: event.id
    }).remove().then(res => {
        return res
    })

    //删除科目记录
    const resultItem = await db.collection('exam_item').doc(event.id).remove().then(res => {
        return res
    })

    return {
        resultScore: resultScore,
        resultItem: resultItem,
    }
}

