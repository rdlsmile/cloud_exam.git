// 云函数入口文件
const cloud = require('wx-server-sdk')
// 初始化 cloud
cloud.init({
    // API 调用都保持和云函数当前所在环境一致
    env: cloud.DYNAMIC_CURRENT_ENV
})

// 云函数入口函数
exports.main = async (event, context) => {
    const db = cloud.database()

    console.log("通过id获取分数参数[event]", event)
    let result = await db.collection('exam_score').doc(event.id).get().then(res => {
        console.log(res)
        return res
    }).catch(error=>{
        console.log(error)
        return null
    })
    return result
}

