// 云函数入口文件
const cloud = require('wx-server-sdk')
// 初始化 cloud
cloud.init({
    // API 调用都保持和云函数当前所在环境一致
    env: cloud.DYNAMIC_CURRENT_ENV
})

// 云函数入口函数
exports.main = async (event, context) => {
    const db = cloud.database()
    let data = {
        openId: event.userInfo.openId,
        appId: event.userInfo.appId,
        name: event.name,
        time: new Date().getTime(),
    }
    console.log("修改科目记录参数[data]:", data, event.id)
    const result = await db.collection('exam_item').doc(event.id).set({
        data:data,
    }).then(res => {
        console.log("修改科目记录返回[res]", res)
        return res
    })
    return result

}

