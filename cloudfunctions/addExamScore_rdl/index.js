// 云函数入口文件
const cloud = require('wx-server-sdk')
// 初始化 cloud
cloud.init({
    // API 调用都保持和云函数当前所在环境一致
    env: cloud.DYNAMIC_CURRENT_ENV
})

// 云函数入口函数
exports.main = async (event, context) => {
    const db = cloud.database()
    let data = {
        openId: event.userInfo.openId,
        appId: event.userInfo.appId,
        itemId: event.itemId,
        itemName: event.itemName,
        time: event.time,
        type: event.type,
        score: event.score,
        note: event.note,
    }
    console.log("添加分数参数[data]", data)
    let result = await db.collection('exam_score').add({
        data: data,
    }).then(res => {
        console.log("添加分数返回[res]", res)
        res.data = data
        return res
    })

    return {
        code: "200",
        data: result,
        message: "成功"
    }
}

