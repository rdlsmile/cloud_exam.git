// 云函数入口文件
const cloud = require('wx-server-sdk')
// 初始化 cloud
cloud.init({
    // API 调用都保持和云函数当前所在环境一致
    env: cloud.DYNAMIC_CURRENT_ENV
})

// 云函数入口函数
exports.main = async (event, context) => {
    const db = cloud.database()
    //查询科目列表
    const itemList = await db.collection('exam_item')
        .where({
            openId: event.userInfo.openId,
        })
        .orderBy('time', 'asc')
        .get()
        .then(res => {
            console.error("查询科目列表返回", res)
            return res.data
        })
        .catch(res => {
            console.error("查询科目列表错误", res)
        })

    return {
        itemList: itemList,
    }
}
