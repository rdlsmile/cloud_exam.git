# 部署教程
### 下载代码

- <img src="https://gitee.com/rdlsmile/cloud_exam/raw/master/img/guide/clone.png" width="375">

### 导入微信开发者工具

- <img src="https://gitee.com/rdlsmile/cloud_exam/raw/master/img/guide/import.png" width="375">

### 修改appId

- <img src="https://gitee.com/rdlsmile/cloud_exam/raw/master/img/guide/appId.png" width="375">

### 选择云环境

- <img src="https://gitee.com/rdlsmile/cloud_exam/raw/master/img/guide/env.png" width="375">

### 上传云函数

- <img src="https://gitee.com/rdlsmile/cloud_exam/raw/master/img/guide/upload.png" width="375">

### 启动页面

- <img src="https://gitee.com/rdlsmile/cloud_exam/raw/master/img/guide/open.png" width="375">